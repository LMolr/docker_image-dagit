FROM python:3.7

ARG PIP_TIMEOUT=1000

ENV DAGSTER_HOME="/srv/run/dagit/dagster_home/"
ENV DAGSTER_WORKSPACE="/srv/run/dagit/dagster_workspace/"
ENV PYTHONPATH="/srv/lib/"

RUN set -eu; \
  set -x; \
  mkdir -p /srv/assets/ ; \
  mkdir -p /srv/lib/ ; \
  mkdir -p /srv/requirements/ ; \
  mkdir -p /srv/run/ ; \
  mkdir -p "${DAGSTER_HOME}" ; \
  mkdir -p "${DAGSTER_WORKSPACE}" ; \
  mkdir -p /srv/run/log/ ; \
  mkdir -p /srv/run/shared/

RUN set -eu; \
  set -x; \
  apt-get update; \
  apt-get install -y --no-install-recommends \
		build-essential \
    ccache \
    cmake; \
	rm -rf /var/lib/apt/lists/*

RUN set -eu; \
  set -x; \
  ln -s /usr/bin/ccache /usr/local/bin/gcc; \
  ccache --set-config=sloppiness=file_macro; \
  ccache --set-config=hash_dir=false

COPY dagit/requirements.txt /srv/requirements/

RUN set -eu; \
  set -x; \
  pip install --no-cache-dir \
    --default-timeout="${PIP_TIMEOUT}" \
    --upgrade pip; \
  pip install --no-cache-dir \
    --default-timeout="${PIP_TIMEOUT}" \
    -r /srv/requirements/requirements.txt

RUN set -eu; \
  set -x; \
  pip freeze --all; \
  pip check

WORKDIR "${DAGSTER_WORKSPACE}"

CMD ["bash", "-c", "dagit -h 0.0.0.0 -p 3000"]
